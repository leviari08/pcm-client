import {Role} from "../enums/role-enum";

export interface userInterface {
  name: string,
  role: Role,
  Cadets: userInterface[]
}
