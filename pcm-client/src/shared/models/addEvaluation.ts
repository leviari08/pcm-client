import {evaluationInterface} from "./evaluation";

export interface addEvaluationInterface {
  course: string,
  series: number,
  syllabus: number,
  evaluation: evaluationInterface
}

