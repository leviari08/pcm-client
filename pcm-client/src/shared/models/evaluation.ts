import {gradeInterface} from "./grade";
import {userInterface} from "./user";
import {ExerciseNames} from "../enums/exercise-enum";

export interface evaluationInterface {
  id: string,
  cadet: userInterface,
  exercises: Exercise[]
  grades: gradeInterface[]
  note: string,
  pros: string[],
  cons: string[],
  generalFeedback: string
}


type Exercise = Record<ExerciseNames, string>

