import {evaluationInterface} from "./evaluation";

export interface syllabusInterface {
  id:number,
  name: string,
  date: Date,
  evaluations: evaluationInterface[]
}
