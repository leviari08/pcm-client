import {CategoryType} from "../enums/category-type-enums";
import {Category} from "../enums/category-enum";

export interface gradeInterface {
  type: CategoryType,
  name: Category,
  value: number
}

