import {seriesInterface} from "./series";
import {userInterface} from "./user";

export interface courseInterface {
  name: string,
  series: seriesInterface[],
  soldiers: userInterface[]
}
