import {syllabusInterface} from "./syllabus";

export interface seriesInterface {
  id: number,
  name: string,
  syllabuses: syllabusInterface[]
}
