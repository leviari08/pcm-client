import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SummaryScreenComponent} from "./summary-screen/summary-screen.component";


@NgModule({
  declarations: [
    SummaryScreenComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SummaryModule { }
