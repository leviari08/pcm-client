import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { distinctUntilChanged } from 'rxjs';
import { Role } from 'src/shared/enums/role-enum';
import { userInterface } from './../../../shared/models/user';

@UntilDestroy()
@Component({
  selector: 'app-login-screen',
  templateUrl: './login-screen.component.html',
  styleUrls: ['./login-screen.component.less']
})
export class LoginScreenComponent implements OnInit {
  users: userInterface[];
  selectedUserNameFormControl = new FormControl();
  filteredOptions: string[] = [];

  constructor() {
    const userMock: userInterface[] = [
      { name: 'יוסי', role: Role.Instructor, Cadets: [] },
      { name: 'מוראד', role: Role.Instructor, Cadets: [] },
      { name: 'מקס', role: Role.Instructor, Cadets: [] },
      { name: 'אריאל', role: Role.Instructor, Cadets: [] }
    ]

    this.users = userMock;
  }

  ngOnInit(): void {
    this.selectedUserNameFormControl.valueChanges.pipe(untilDestroyed(this), distinctUntilChanged())
      .subscribe(value =>
        this.filteredOptions = this.users.map(user => user.name).filter(name => name.includes(value))
      );

    this.selectedUserNameFormControl.setValue('');

    // this.users = getUsersFromStore();
  }

  login(): void {
    const userExists = this.users.map(user => user.name).includes(this.selectedUserNameFormControl.value)

    if (!userExists)
      return

    alert(this.selectedUserNameFormControl.value)
    // save user to store
    // this.users = getUsersFromStore();
  }
}
