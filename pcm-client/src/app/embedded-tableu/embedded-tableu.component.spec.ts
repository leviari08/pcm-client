import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbeddedTableuComponent } from './embedded-tableu.component';

describe('EmbeddedTableuComponent', () => {
  let component: EmbeddedTableuComponent;
  let fixture: ComponentFixture<EmbeddedTableuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmbeddedTableuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbeddedTableuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
