import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {StoreModule} from '@ngrx/store';
import {TableauModule} from 'ngx-tableau';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CoursesModule} from "./courses/courses.module";
import {EmbeddedTableuComponent} from './embedded-tableu/embedded-tableu.component';
import {LoginModule} from './login/login.module';
import {NavbarModule} from "./shared/navbar.module";
import {StudentsModule} from "./students/students.module";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";

@NgModule({
  declarations: [
    AppComponent,
    EmbeddedTableuComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot({}, {}),
    BrowserAnimationsModule,
    CoursesModule,
    StudentsModule,
    LoginModule,
    TableauModule,
    NavbarModule,
    StoreDevtoolsModule.instrument(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
