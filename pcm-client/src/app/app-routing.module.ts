import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {CoursesListComponent} from "./courses/courses-list/courses-list.component";
import {SummaryScreenComponent} from "./summary/summary-screen/summary-screen.component";
import {EmbeddedTableuComponent} from "./embedded-tableu/embedded-tableu.component";
import {StudentsPageComponent} from "./students/students-page/students-page.component";

const routes: Routes = [
  {path: 'students', component: StudentsPageComponent},
  {path: 'courses', component: CoursesListComponent},
  {path: 'summary', component: SummaryScreenComponent},
  {path: 'tableu-summary', component: EmbeddedTableuComponent}
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
