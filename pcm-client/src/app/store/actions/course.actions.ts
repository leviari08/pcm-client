import {courseInterface} from "../../../shared/models/course";
import {createAction, props} from "@ngrx/store";
import {addEvaluationInterface,} from "../../../shared/models/addEvaluation";

export const addCourse = createAction('[Course State] Add Course', props<{ course: courseInterface }>())
export const addEvaluation = createAction('[Course State] Add Evaluation', props<{ evaluation: addEvaluationInterface }>())
export const editEvaluation = createAction('[Course State] Edit Evaluation', props<{ evaluation: addEvaluationInterface }>())
