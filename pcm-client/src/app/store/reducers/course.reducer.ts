import {courseInterface} from "../../../shared/models/course";
import {createReducer, createSelector, on} from "@ngrx/store";
import {courseMock} from "../../../shared/course-mock";
import * as CourseActions from "../actions/course.actions";
import {seriesInterface} from "../../../shared/models/series";
import {syllabusInterface} from "../../../shared/models/syllabus";
import {produce} from "immer"
import {evaluationInterface} from "../../../shared/models/evaluation";


export const courseFeatureKey = 'CourseState';

export interface CoursesState {
  courses: courseInterface[]
}

export const initialState: CoursesState = {courses: [courseMock]}

export const courseReducer = createReducer(initialState,
  on(CourseActions.addCourse, (state, {course}) => ({
    ...state,
    courses: [...state.courses, course]
  })),
  on(CourseActions.addEvaluation, (state, {evaluation}) => {
      const courseIndex: number = state.courses.findIndex((course: courseInterface) => course.name === evaluation.course)
      const seriesIndex: number = state.courses[courseIndex].series.findIndex((series: seriesInterface) => series.id === evaluation.series)
      const syllabusIndex: number = state.courses[courseIndex].series[seriesIndex].syllabuses.findIndex((syllabus: syllabusInterface) => syllabus.id === evaluation.syllabus)

      return produce(state, (draftState: courseInterface[]) => {
        draftState[courseIndex].series[seriesIndex].syllabuses[syllabusIndex].evaluations.push(evaluation.evaluation)
      })
    }
  ), on(CourseActions.editEvaluation, (state, {evaluation}) => {
    const courseIndex: number = state.courses.findIndex((course: courseInterface) => course.name === evaluation.course)
    const seriesIndex: number = state.courses[courseIndex].series.findIndex((series: seriesInterface) => series.id === evaluation.series)
    const syllabusIndex: number = state.courses[courseIndex].series[seriesIndex].syllabuses.findIndex((syllabus: syllabusInterface) => syllabus.id === evaluation.syllabus)
    const evaluationIndex: number = state.courses[courseIndex].series[seriesIndex].syllabuses[syllabusIndex].evaluations
      .findIndex((currEval: evaluationInterface) => currEval.id === evaluation.evaluation.id)
    return produce(state, (draftState: courseInterface[]) => {
      draftState[courseIndex].series[seriesIndex].syllabuses[syllabusIndex].evaluations[evaluationIndex] = evaluation.evaluation
    })
  })
)


export const selectCourses = (state: CoursesState) => state.courses;
export const selectCourse = createSelector(selectCourses, (courses: courseInterface[], props: any) => courses.find(course => course.name === props.courseName));
