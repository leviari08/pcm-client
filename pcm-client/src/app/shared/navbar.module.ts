import { NgModule } from '@angular/core';
import { NavbarComponent } from './navbar/navbar.component';
import {MatBadgeModule} from "@angular/material/badge";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {AppRoutingModule} from "../app-routing.module";
import {BrowserModule} from "@angular/platform-browser";

const NAVBAR_MATERIAL_MODULES = [
  MatButtonModule,
  MatButtonToggleModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatBadgeModule,
  MatToolbarModule
];


@NgModule({
  declarations: [
    NavbarComponent
  ],
  imports: [
    NAVBAR_MATERIAL_MODULES,
    AppRoutingModule,
    BrowserModule
  ],
  exports: [
    NAVBAR_MATERIAL_MODULES,
    NavbarComponent
  ]

})
export class NavbarModule { }
