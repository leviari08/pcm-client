import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-navbar-component',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.less'],
})
export class NavbarComponent implements OnInit {
  siteTitle = "מערכת לניהול הכשרת מפעילי כטממ"

  navbarButtons = [
    {link: "/courses", title: "קורסים"},
    {link: "/students", title: "חניכים"},
    {link: "/tableu-summary", title: "סיכומים"}
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
