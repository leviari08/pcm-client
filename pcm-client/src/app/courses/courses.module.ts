import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CoursesListComponent} from "./courses-list/courses-list.component";
import {MatExpansionModule} from "@angular/material/expansion";
import {CourseComponent} from './course/course.component';
import {SeriesComponent} from './series/series.component';
import {MatButtonModule} from '@angular/material/button';
import {SyllabusComponent} from './syllabus/syllabus.component';
import {StudentComponent} from './student/student.component';
import {StoreModule} from "@ngrx/store";
import {courseFeatureKey, courseReducer} from "../store/reducers/course.reducer";


@NgModule({
  declarations: [
    CoursesListComponent,
    CourseComponent,
    SeriesComponent,
    SyllabusComponent,
    StudentComponent
  ],
  exports: [
    CoursesListComponent,
    StudentComponent
  ],
  imports: [
    CommonModule,
    StoreModule.forFeature(courseFeatureKey, courseReducer),
    CommonModule,
    MatExpansionModule,
    MatButtonModule]
})

export class CoursesModule {
}
