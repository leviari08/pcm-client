import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-syllabus',
  templateUrl: './syllabus.component.html',
  styleUrls: ['./syllabus.component.less']
})
export class SyllabusComponent implements OnInit {
  @Input() syllabus: string;
  studentsList: string[];

  constructor() {
    this.studentsList = ['רון', 'תמיר', 'לורן'];
    this.syllabus = '';
  }

  ngOnInit(): void {
  }

}
