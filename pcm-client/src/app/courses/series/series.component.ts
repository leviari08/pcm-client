import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.less']
})
export class SeriesComponent implements OnInit {
  @Input() series: string;
  syllabusList: string[];

  constructor() {
    this.series = ''
    this.syllabusList = ['סילבוס 1', 'סילבוס 2', 'סילבוס 3'];

  }

  ngOnInit(): void {
  }

}
