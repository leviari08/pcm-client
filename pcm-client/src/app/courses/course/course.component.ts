import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.less']
})
export class CourseComponent implements OnInit {
  @Input() courseName: string
  seriesList: string[];

  constructor() {
    this.courseName = '';
    this.seriesList = ['סדרה 1', 'סדרה 2', 'סדרה 3']

  }

  ngOnInit(): void {
  }

}
