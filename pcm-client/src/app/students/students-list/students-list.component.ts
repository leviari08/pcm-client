import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.less']
})
export class StudentsListComponent implements OnInit {
  studentsList:string[];
  constructor() {
    this.studentsList = ['תמיר','לורן','רון','אריאל','בן','מקס']
  }

  ngOnInit(): void {
  }

}
