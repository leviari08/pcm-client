import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StudentsListComponent} from './students-list/students-list.component';
import {SearchBarComponent} from './search-bar/search-bar.component';
import {StudentsPageComponent} from './students-page/students-page.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {CoursesModule} from "../courses/courses.module";

@NgModule({
  declarations: [
    StudentsListComponent,
    SearchBarComponent,
    StudentsPageComponent
  ],
  exports: [
    StudentsListComponent
  ],
  imports: [
    CommonModule, MatFormFieldModule, MatIconModule, MatInputModule, CoursesModule
  ]
})
export class StudentsModule {
}
